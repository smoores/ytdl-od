import "https://deno.land/std@0.208.0/dotenv/load.ts";

import {
  encodeBase64Url,
  decodeBase64Url,
} from "https://deno.land/std@0.208.0/encoding/base64url.ts";
import { basename } from "https://deno.land/std@0.208.0/path/basename.ts";
import { tailLine } from "https://deno.land/x/tail_lines@v0.5.2/mod.ts";
import { mergeReadableStreams } from "https://deno.land/std@0.200.0/streams/merge_readable_streams.ts";
import {
  Application,
  Router,
  Status,
  RouterContext,
  ServerSentEvent,
  HttpError,
  createHttpError,
} from "https://deno.land/x/oak@v12.6.1/mod.ts";

const DATA_DIR = Deno.env.get("YTDL_DATA_DIR") ?? Deno.cwd();
const YTDL_COMMAND = Deno.env.get("YTDL_COMMAND") ?? "yt-dlp";
const YTDL_ARGSTRING =
  Deno.env.get("YTDL_ARGS") ?? "--config-location /config/ytdlp.conf";
const YTDL_ARGS = YTDL_ARGSTRING.split(" ");

const COMPLETE_STRING = "[complete]";

const router = new Router();

router.post("/video", async (context) => {
  const params = await context.request.body({
    type: "form",
  }).value;

  const url = params.get("url")!;
  const base64Url = encodeBase64Url(new TextEncoder().encode(url));

  const command = new Deno.Command(YTDL_COMMAND, {
    args: [
      ...YTDL_ARGS,
      "-q",
      "--progress",
      "--newline",
      "--print",
      `after_video:${COMPLETE_STRING}`,
      "--print",
      "after_move:[DOWNLOADED_TO=%(filename)s]",
      url,
    ],
    stdout: "piped",
    stderr: "piped",
  });

  const process = command.spawn();
  const joined = mergeReadableStreams(process.stdout, process.stderr);

  const file = await Deno.open(`${DATA_DIR}/progress/${base64Url}`, {
    write: true,
    read: true,
    create: true,
  });

  joined.pipeTo(file.writable);

  context.response.redirect("/");
});

router.get("/download/:video", async (context) => {
  const [secondToLastLine, lastLine] = await tailLine(
    `${DATA_DIR}/progress/${context.params.video}`,
    2
  );

  const status = lastLine.includes("[download]")
    ? "downloading"
    : lastLine.includes(COMPLETE_STRING)
    ? "completed"
    : "error";

  if (status !== "completed")
    throw createHttpError(
      Status.BadRequest,
      "Video not available for download"
    );

  const downloadedTo =
    secondToLastLine.match(/\[DOWNLOADED_TO=(.*)\]/)?.[1] ?? null;

  if (downloadedTo === null)
    throw createHttpError(
      Status.BadRequest,
      "Video not available for download"
    );

  await context.send({ path: downloadedTo, root: "." });
});

router.get("/videos", async (context) => {
  const base64Urls = Deno.readDir(`${DATA_DIR}/progress`);

  const urls: Array<{
    url: string;
    status: "error" | "downloading" | "completed";
    progress: string;
    downloadedTo: string | null;
  }> = [];
  for await (const base64Url of base64Urls) {
    if (!base64Url.isFile) continue;

    let url: string;
    try {
      url = new TextDecoder().decode(decodeBase64Url(base64Url.name));
    } catch {
      continue;
    }

    const [secondToLastLine, lastLine] = await tailLine(
      `${DATA_DIR}/progress/${base64Url.name}`,
      2
    );

    if (lastLine === undefined) {
      urls.push({
        url,
        status: "downloading",
        progress: "0.0%",
        downloadedTo: null,
      });
      continue;
    }

    const status = lastLine.includes("[download]")
      ? "downloading"
      : lastLine.includes(COMPLETE_STRING)
      ? "completed"
      : "error";

    const downloadedTo =
      status === "completed"
        ? secondToLastLine.match(/\[DOWNLOADED_TO=(.*)\]/)?.[1] ?? null
        : null;

    const match = lastLine.match(/\[download\]\s*([0-9.]*%)/);

    const progress = lastLine.includes(COMPLETE_STRING)
      ? "100%"
      : match?.[1]
      ? match[1]
      : "?";

    urls.push({
      url,
      status,
      progress,
      downloadedTo: downloadedTo ? basename(downloadedTo) : null,
    });
  }

  context.response.headers.append("Content-Type", "application/json");
  context.response.status = 200;
  context.response.body = urls;
});

router.get(
  "/video/progress-events/:base64url",
  (
    context: RouterContext<
      "/video/progress-events/:base64url",
      {
        base64url: string;
      }
    >
  ) => {
    context.assert(
      context.request.accepts("text/event-stream"),
      Status.UnsupportedMediaType
    );

    const base64Url = context.params.base64url;
    const url = new TextDecoder().decode(decodeBase64Url(base64Url));

    const target = context.sendEvents();

    const watcher = Deno.watchFs(`${DATA_DIR}/progress/${base64Url}`);

    async function watch() {
      for await (const event of watcher) {
        if (event.kind !== "modify") continue;

        const [lastLine] = await tailLine(
          `${DATA_DIR}/progress/${base64Url}`,
          1
        );
        const status = lastLine.includes("[download]")
          ? "downloading"
          : lastLine.includes(COMPLETE_STRING)
          ? "completed"
          : "error";

        const match = lastLine.match(/\[download\]\s*([0-9.]*%) of/);
        const progress = lastLine.includes(COMPLETE_STRING)
          ? "100%"
          : match?.[1]
          ? match[1]
          : "?";

        const serverEvent = new ServerSentEvent("message", {
          data: { url, status, progress },
        });
        target.dispatchEvent(serverEvent);

        if (status === "completed") {
          return;
        }
      }
    }

    watch();
  }
);

const app = new Application();

app.use(router.routes());
app.use(router.allowedMethods());

app.use(async (context, next) => {
  try {
    await context.send({
      root: `${Deno.cwd()}/public`,
      index: "index.html",
    });
  } catch {
    await next();
  }
});

await app.listen({ hostname: "0.0.0.0", port: 8000 });
