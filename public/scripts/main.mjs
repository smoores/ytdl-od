import {
  encodeBase64Url,
} from "https://est.deno.dev/https://deno.land/std@0.208.0/encoding/base64url.ts";


async function main() {
  const inProgressSection = document.querySelector('[data-video-list="in-progress"]')
  const completedSection = document.querySelector('[data-video-list="completed"]')

  const response = await fetch('/videos', { headers: { 'Accept': 'application/json' } })
  /** @type {Array<{url: string; status: 'downloading' | 'error' | 'completed'; progress: string, downloadedTo: string | null}>} */
  const videos = await response.json()

  for (const video of videos) {
    const { url, status, progress, downloadedTo } = video
    const base64Url = encodeBase64Url(new TextEncoder().encode(url))

    const progressElement = document.createElement("div")
    const textNode = document.createTextNode(status === 'completed' ? downloadedTo : `${url} - ${progress}`)

    progressElement.appendChild(textNode)
    progressElement.dataset.videoId = base64Url

    if (status === 'completed') {
      const downloadLink = document.createElement('a')
      downloadLink.href = `/download/${base64Url}`
      downloadLink.download = downloadedTo
      downloadLink.appendChild(document.createTextNode('Download'))
      progressElement.appendChild(document.createTextNode(' '))
      progressElement.appendChild(downloadLink)
    }

    if (status === 'completed') {
      completedSection.appendChild(progressElement)
      continue
    }

    if (status === 'downloading') {
      inProgressSection.appendChild(progressElement)
      const eventSource = new EventSource(`/video/progress-events/${base64Url}`)
      eventSource.addEventListener('message', (event) => {
        /** @type {{url: string; status: 'downloading' | 'error' | 'completed'; progress: string}} */
        const update = JSON.parse(event.data)
        const updateTextNode = document.createTextNode(`${update.url} - ${update.progress}`)
        progressElement.replaceChildren(updateTextNode)

        if (update.status === 'completed') {
          completedSection.appendChild(progressElement)
        }
      })
    }
  }
}

main()
