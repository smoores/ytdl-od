ARG DENO_VERSION=1.38.5
ARG BIN_IMAGE=denoland/deno:bin-${DENO_VERSION}


FROM ${BIN_IMAGE} AS bin


FROM buildpack-deps:20.04-curl AS tini

ARG TINI_VERSION=0.19.0
RUN curl -fsSL https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini \
  --output /tini \
  && chmod +x /tini


FROM frolvlad/alpine-glibc:alpine-3.17

RUN addgroup --gid 1000 deno \
  && adduser --uid 1000 --disabled-password deno --ingroup deno \
  && mkdir /deno-dir/ \
  && chown deno:deno /deno-dir/

ENV DENO_DIR /deno-dir/
ENV DENO_INSTALL_ROOT /usr/local

ARG DENO_VERSION
ENV DENO_VERSION=${DENO_VERSION}
COPY --from=bin /deno /bin/deno

COPY --from=tini /tini /tini

ARG BUILD_VERSION=2024.03.10

RUN set -x \
  && apk update \
  && apk upgrade -a \
  && apk add --no-cache \
  ca-certificates \
  curl \
  ffmpeg \
  python3 \
  py3-mutagen \
  # Install youtube-dl
  && curl -Lo /usr/local/bin/yt-dlp https://github.com/yt-dlp/yt-dlp/releases/download/${BUILD_VERSION}/yt-dlp \
  && chmod a+rx /usr/local/bin/yt-dlp \
  # Clean-up
  && apk del curl \
  # Create directory to hold downloads.
  && mkdir /downloads \
  && chmod a+rw /downloads \
  # Sets up cache.
  && mkdir /.cache \
  && chmod 777 /.cache

ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt

COPY ./_entry.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod 755 /usr/local/bin/docker-entrypoint.sh

COPY main.ts deno.json deno.lock /app/
COPY public/ /app/public/

WORKDIR /app

ENTRYPOINT ["/tini", "--", "docker-entrypoint.sh", "run", "--allow-read", "--allow-write", "--allow-env", "--allow-net", "--allow-run", "/app/main.ts"]
CMD ["eval", "console.log('Welcome to Deno!')"]
